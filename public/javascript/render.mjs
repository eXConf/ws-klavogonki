import { createElement } from "./domHelper.mjs";
const username = sessionStorage.getItem("username");

export function renderRooms(socket, rooms) {
  const roomsContainer = document.getElementById("rooms-page");
  roomsContainer.innerHTML = "";

  rooms.forEach(room => {
    const roomEl = createRoomElement(socket, room);
    roomsContainer.appendChild(roomEl);
  });

  const newRoomBtn = createNewRoomBtn(socket);
  roomsContainer.appendChild(newRoomBtn);
}

export function togglePages() {
  toggleRoomsPage();
  toggleGamePage();
}

export function toggleRoomsPage() {
  const roomsPage = document.getElementById("rooms-page");
  toggleElement(roomsPage);
}

export function toggleGamePage() {
  const gamePage = document.getElementById("game-page");
  toggleElement(gamePage);
}

function createRoomElement(socket, room) {
  const { name, playersCount } = room;

  const roomEl = createRoomEl();
  const infoEl = createInfoEl(name, playersCount);
  const buttonEl = createButtonEl(socket, name);

  roomEl.appendChild(infoEl);
  roomEl.appendChild(buttonEl);

  return roomEl;

  function createRoomEl() {
    const roomEl = createElement({
      tagName: "div",
      className: "room flex-centered flex-column",
    });

    return roomEl;
  }

  function createInfoEl(roomName, playersCount) {
    const infoEl = createElement({
      tagName: "div",
      className: "room-info",
    });

    infoEl.innerText = `${roomName}: ${playersCount}`;

    return infoEl;
  }

  function createButtonEl(socket, roomName) {
    const buttonEl = createElement({
      tagName: "button",
      className: "join-btn btn",
    });

    buttonEl.innerText = "Join";
    buttonEl.addEventListener("click", () => {
      socket.emit("JOIN_ROOM", roomName);
    });

    return buttonEl;
  }
}

function createNewRoomBtn(socket) {
  const newRoomBtn = createElement({
    tagName: "button",
    className: "add-room-btn btn",
  });

  newRoomBtn.innerText = "Create Room";
  newRoomBtn.addEventListener("click", () => {
    const newRoomName = prompt("Please enter a name for the new room");
    socket.emit("CREATE_ROOM", newRoomName);
  });

  return newRoomBtn;
}

function toggleElement(element) {
  const isHidden = element.classList.contains("display-none");

  if (isHidden) {
    element.classList.remove("display-none");
    element.classList.add("flex");
    return;
  }

  element.classList.remove("flex");
  element.classList.add("display-none");
}

export function updatePlayersList(players) {
  const playersList = document.getElementById("players-list");
  playersList.innerHTML = "";

  players.forEach(player => {
    const playerEl = createPlayer(player);
    playersList.appendChild(playerEl);
  });

  function createPlayer(player) {
    const playerWrapper = createElement({
      tagName: "div",
      className: "player-wrapper",
    });

    const nameWrapper = createElement({
      tagName: "div",
      className: "name-wrapper flex-left-centered flex-row",
    });

    const status = createElement({
      tagName: "div",
      className: `ready-status ${
        player.isReady ? "ready-status-green" : "ready-status-red"
      }`,
    });

    const name = createElement({
      tagName: "div",
      className: "player-name",
    });
    name.innerText = player.name;

    const progressbarWrapper = createElement({
      tagName: "div",
      className: "progressbar-wrapper",
    });

    const progressbar = createElement({
      tagName: "div",
      className: `user-progress ${player.name}`,
    });
    progressbar.style.width = `${player.progress}%`;

    playerWrapper.appendChild(nameWrapper);
    nameWrapper.appendChild(status);
    nameWrapper.appendChild(name);
    playerWrapper.appendChild(progressbarWrapper);
    progressbarWrapper.appendChild(progressbar);

    return playerWrapper;
  }
}

export function updateRoomInfo(socket, room) {
  const roomNameEl = document.getElementById("room-name");
  roomNameEl.innerText = room.name;

  addListeners(socket, room)
}

export function addListeners(socket, room) {
  addBackButtonListener()
  addReadyButtonListener()

  function addBackButtonListener() {
    const backButton = document.getElementById("quit-room-btn");
    backButton.addEventListener("click", () => {
      socket.emit("USER_LEFT_ROOM", room.name);
      togglePages();
      removeListeners();
    })
  }

  function addReadyButtonListener() {
    const readyButton = document.getElementById("ready-btn");
    readyButton.addEventListener('click', () => {
      if (readyButton.classList.contains('set-ready')) {
        readyButton.classList.replace('set-ready', 'set-not-ready')
        readyButton.innerText = 'NOT READY'
        socket.emit('USER_READY', room.name)
        return
      }
      readyButton.classList.replace('set-not-ready', 'set-ready')
      readyButton.innerText = 'READY'
      socket.emit('USER_NOT_READY', room.name)
    })
  }
}

function removeListeners() {
  removeBackButtonListeners();
  removeReadyButtonListener();

  function removeBackButtonListeners() {
    const backButton = document.getElementById("quit-room-btn");
    backButton.replaceWith(backButton.cloneNode(true));
  }

  function removeReadyButtonListener() {
    const readyButton = document.getElementById("ready-btn");
    readyButton.replaceWith(readyButton.cloneNode(true));
  }
}