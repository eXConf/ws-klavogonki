import { createElement } from "./domHelper.mjs";
import {
  renderRooms,
  togglePages,
  updatePlayersList,
  updateRoomInfo,
  addListeners,
} from "./render.mjs";

const username = sessionStorage.getItem("username");
const roomsContainer = document.getElementById("rooms-page");

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

function onUserAlreadyExist() {
  sessionStorage.removeItem("username");
  alert("A user with the same name already exists on the server");
  window.location.replace("/login");
}

function onRoomAlreadyExist() {
  alert("Sorry, this room name is already taken. Please choose another one");
}

function onRoomIsFull() {
  alert("Sorry, the room you are trying to connect is full");
}

function onRoomsList(rooms) {
  renderRooms(socket, rooms);
}

function onJoinedRoom(room) {
  updateRoomInfo(socket, room);
  togglePages();
  onPlayersUpdate(room.players);
}

function onPlayersUpdate(players) {
  updatePlayersList(players);
}

function onGameStarted(textId) {
  console.log(textId);
}

socket.on("USER_ALREADY_EXISTS", onUserAlreadyExist);
socket.on("ROOM_ALREADY_EXISTS", onRoomAlreadyExist);
socket.on("ROOM_IS_FULL", onRoomIsFull);
socket.on("ROOMS_LIST", onRoomsList);
socket.on("JOINED_ROOM", onJoinedRoom);
// socket.on("ROOM_UPDATE", onRoomUpdate);
socket.on("PLAYERS_UPDATE", onPlayersUpdate);
socket.on("GAME_STARTED", onGameStarted);
