export const CREATE_ROOM = "CREATE_ROOM";
export const ROOM_ALREADY_EXISTS = "ROOM_ALREADY_EXISTS";
export const JOIN_ROOM = "JOIN_ROOM";
export const JOINED_ROOM = "JOINED_ROOM";
export const ROOM_IS_FULL = "ROOM_IS_FULL";
export const REQUEST_ROOMS_LIST = "REQUEST_ROOMS_LIST";

export const USER_ALREADY_EXISTS = "USER_ALREADY_EXISTS";
export const USER_LEFT_ROOM = "USER_LEFT_ROOM";
export const USER_READY = "USER_READY";
export const USER_NOT_READY = "USER_NOT_READY";

export const ROOMS_LIST = "ROOMS_LIST";
export const PLAYERS_UPDATE = "PLAYERS_UPDATE";

export const GAME_STARTED = "GAME_STARTED";