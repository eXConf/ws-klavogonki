import * as events from "../socket/events";
import { Lobby } from "../services";

export default io => {
  const lobby = new Lobby(io);
  
  io.on("connection", socket => {
    lobby.addPlayer(socket);

    socket.on("disconnect", () => {
      lobby.removePlayer(socket);
    });

    socket.on(events.CREATE_ROOM, roomName => {
      lobby.createRoom(socket, roomName);
    });

    socket.on(events.JOIN_ROOM, roomName => {
      lobby.sendPlayerToRoom(socket, roomName);
    });

    socket.on(events.USER_LEFT_ROOM, roomName => {
      lobby.removePlayerFromRoom(socket, roomName);
    });

    socket.on(events.USER_READY, roomName => {
      lobby.setPlayerReady(socket, roomName);
    });

    socket.on(events.USER_NOT_READY, roomName => {
      lobby.setPlayerNotReady(socket, roomName);
    });
  });
};
