import * as config from "../socket/config";
import * as events from "../socket/events";

class Room {
  constructor(name, io) {
    this.name = name;
    this.players = [];
    this.isGameStarted = false;
    this.io = io;
  }

  addPlayer(socket) {
    if (this.getPlayersCount() === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
      return socket.emit(events.ROOM_IS_FULL);
    }

    const player = this.players.find(
      el => el.name === socket.handshake.query.username
    );

    if (player) {
      return socket.emit(events.JOINED_ROOM, this.getRoomInfo());
    }

    this.players.push({
      socket,
      name: socket.handshake.query.username,
      isReady: false,
      progress: 0,
    });
    socket.join(this.name);
    socket.emit(events.JOINED_ROOM, this.getRoomInfo());
    socket.to(this.name).emit(events.PLAYERS_UPDATE, this.getPlayersList());
  }

  removePlayer(socket) {
    this.players = this.players.filter(
      el => el.name !== socket.handshake.query.username
    );
    socket.to(this.name).emit(events.ROOM_UPDATE, this.getRoomInfo());
    this.checkAllPlayersReady();
  }

  getPlayersList() {
    const players = this.players.map(player => {
      return {
        name: player.name,
        isReady: player.isReady,
        progress: player.progress,
      };
    });

    return players;
  }

  getPlayersCount() {
    return this.players.length;
  }

  setPlayerReady(socket) {
    const { username } = socket.handshake.query;
    const player = this.players.find(el => el.name === username);
    player.isReady = true;

    this.sendPlayersUpdate(socket);
    this.checkAllPlayersReady();
  }

  setPlayerNotReady(socket) {
    const { username } = socket.handshake.query;
    const player = this.players.find(el => el.name === username);
    player.isReady = false;

    this.sendPlayersUpdate();
  }

  checkAllPlayersReady() {
    const readyPlayers = this.players.filter(el => el.isReady === true);
    const readyCount = readyPlayers.length;;

    if (readyCount >= 1 && readyCount === this.players.length) {
      this.startGame();
    }
  }

  startGame() {
    const randomTextId = Math.floor(Math.random() * 10);
    this.io.to(this.name).emit(events.GAME_STARTED, randomTextId);
  }

  sendPlayersUpdate() {
    this.io.to(this.name).emit(events.PLAYERS_UPDATE, this.getPlayersList());
  }

  getRoomInfo() {
    const info = {
      name: this.name,
      players: this.getPlayersList(),
      isStarted: this.isGameStarted,
    };

    return info;
  }

  getRoomName() {
    return this.name;
  }
}

export default Room;
