import { Room } from "./index";
import * as config from "../socket/config";
import * as events from "../socket/events";

class Lobby {
  constructor(io) {
    this.players = [];
    this.rooms = [];
    this.io = io;
  }

  isPlayerExists(socket) {
    const username = socket.handshake.query.username;
    const player = this.players.find(
      el => el.handshake.query.username === username
    );

    return Boolean(player);
  }

  isRoomExists(roomName) {
    const room = this.rooms.find(room => room.getRoomName() === roomName);

    return Boolean(room);
  }

  addPlayer(socket) {
    if (this.isPlayerExists(socket)) {
      return socket.emit(events.USER_ALREADY_EXISTS);
    }

    this.players.push(socket);
    return socket.emit(events.ROOMS_LIST, this.getRooms());
  }

  sendPlayerToRoom(socket, roomName) {
    const room = this.rooms.find(el => el.getRoomName() === roomName);

    if (!room) return;

    room.addPlayer(socket);
    socket.broadcast.emit(events.ROOMS_LIST, this.getRooms());
  }

  removePlayerFromRoom(socket, roomName) {
    const room = this.rooms.find(el => el.getRoomName() === roomName);

    if (!room) return;

    room.removePlayer(socket);
    socket.emit(events.ROOMS_LIST, this.getRooms());
    socket.broadcast.emit(events.ROOMS_LIST, this.getRooms());
  }

  createRoom(socket, roomName) {
    if (this.isRoomExists(roomName)) {
      return socket.emit(events.ROOM_ALREADY_EXISTS);
    }

    this.rooms.push(new Room(roomName, this.io));

    this.sendPlayerToRoom(socket, roomName);
    socket.broadcast.emit(events.ROOMS_LIST, this.getRooms());
  }

  removePlayer(socket) {
    this.players = this.players.filter(
      el => el.handshake.query.username !== socket.handshake.query.username
    );
  }

  setPlayerReady(socket, roomName) {
    const room = this.rooms.find(el => el.name === roomName);
    if (!room) return;

    room.setPlayerReady(socket);
  }

  setPlayerNotReady(socket, roomName) {
    const room = this.rooms.find(el => el.name === roomName);
    if (!room) return;

    room.setPlayerNotReady(socket);
  }

  getRooms() {
    const rooms = this.rooms.map(room => {
      return {
        name: room.getRoomName(),
        playersCount:
          room.getPlayersCount() + "/" + config.MAXIMUM_USERS_FOR_ONE_ROOM,
      };
    });

    return rooms;
  }

  getPlayers() {
    return [...this.players];
  }
}

export default Lobby;
